-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 02, 2019 at 08:43 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mg_tech`
--

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `brand_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `image` varchar(50) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`brand_id`, `name`, `image`, `status`, `created`, `updated`) VALUES
(1, 'ZTE', '2.jpg', 1, '2018-11-02 00:00:00', '2018-12-01 00:00:00'),
(2, 'DEVISAR', '3.jpg', 1, '2018-11-06 00:00:00', '2018-12-01 00:00:00'),
(3, 'POWERLINK', '4.jpg', 1, '2018-09-01 00:00:00', '2018-12-01 00:00:00'),
(4, 'PREVAIL', '5.jpg', 1, '2018-09-01 00:00:00', '2018-12-05 00:00:00'),
(5, 'RANTEX', '6.jpg', 1, '2018-09-01 00:00:00', '2018-12-05 00:00:00'),
(6, 'ASELSAN', '1.jpg', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'AMPHENO', '25.jpg', 1, '2019-03-13 00:00:00', '2019-03-19 00:00:00'),
(8, 'TRENDnet', '24.jpg', 1, '2019-03-13 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` enum('catv','networking') NOT NULL,
  `icon` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `name`, `type`, `icon`) VALUES
(1, 'EDFA', 'catv', 'fa-tachometer'),
(2, 'Optical Transmitter', 'catv', 'fa-book'),
(3, 'Optical Receiver', 'catv', 'fa-briefcase'),
(4, 'Optical Power Metter', 'catv', 'fa-newspaper-o'),
(5, 'Optical Fiber Splitter', 'catv', 'fa-shopping-basket'),
(6, 'Optical Patch Cord', 'catv', 'fa-graduation-cap'),
(7, 'Satellite Receiver', 'catv', 'fa-building'),
(8, 'Optical Tools', 'catv', 'fa-diamond'),
(9, 'Laser/Gain IC', 'catv', 'fa-life-ring'),
(10, 'Splicer Machine', 'catv', 'fa-graduation-cap'),
(11, 'LNB', 'catv', 'fa-life-ring'),
(12, 'Management Switch', 'networking', 'fa-tasks'),
(13, 'G/EPON OLT', 'networking', 'fa-user'),
(14, 'G/EPON ONU', 'networking', 'fa-university'),
(15, 'SFP Module', 'networking', 'fa-tasks'),
(16, 'WDM Splitter', 'networking', 'fa-tachometer'),
(17, 'PLC Splitter', 'networking', 'fa-shopping-basket'),
(18, 'Router', 'networking', 'fa-newspaper-o');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `image` varchar(20) NOT NULL,
  `price` varchar(20) NOT NULL,
  `category_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `model_no` varchar(100) NOT NULL,
  `top_rated` tinyint(4) NOT NULL DEFAULT 0,
  `best_selling` tinyint(4) NOT NULL DEFAULT 0,
  `description` text NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `name`, `image`, `price`, `category_id`, `brand_id`, `model_no`, `top_rated`, `best_selling`, `description`, `created`, `updated`) VALUES
(1, 'Multicom – MUL-OTDR-1100 – OTDR Optical Time Domain Reflectometer', '1.jpg', '525.00', 1, 1, 'MUL-OTDR-1100 ', 1, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Multicom – MUL-FSPLICE-200 – Fiber Optic Fusion Splicer', '2.jpg', '550.00', 2, 2, 'MUL-FSPLICE-200', 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Multicom – MUL-FO-CLEAV-100 – High Precision Fiber Optic Cleaver', '3.jpg', '2700.00', 3, 3, 'MUL-FO-CLEAV-100 ', 1, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, '\r\nFOXCOM – AL5T-2xxx – 950-2500 Mhz, DBS/DTH L-Band Transmitter', '4.jpg', '5080.00', 4, 4, 'AL5T-2xxx –950-2500 Mhz', 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Multicom – M660-BVV – RG-6 60% AL Braid – CATV UL', '5.jpg', '3000.00', 5, 5, 'M660-BVV – RG-6 60% ', 1, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Multicom – MDCSA16G – Directional Coupler 16dB', '6.jpg', '2300.00', 18, 6, 'MDCSA16G ', 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Multicom – Part# MP-PA-6-T – Trunk Connector, M-180 Adapter with 6″ Extension', '7.jpg', '550000', 17, 7, ' MP-PA-6-T ', 1, 0, '', '2018-09-01 00:00:00', '2018-12-05 00:00:00'),
(11, 'JTEL-25 - Telephone Cord, 25 FT', '8.jpg', '25000.00', 16, 8, 'TEL-25 - Telephone Cord, 25 FT', 0, 0, '', '2018-09-01 00:00:00', '2018-12-05 00:00:00'),
(12, 'Preformed Line Products - P12SCA3 - Pigtail Assembly, 12 Fiber', '9.jpg', '450', 15, 1, 'P12SCA3', 0, 1, '', '2018-12-12 07:18:34', '2018-12-12 07:18:34'),
(15, 'Multicom - MCA-30860R - 30-860 MHz 30 dB Distribution Amplifier w/ Reverse', '10.jpg', '585.00', 14, 2, 'MCA-30860R - 30-860 MHz 30 dB', 0, 0, '', '2018-12-14 09:01:39', '2018-12-14 09:01:39'),
(16, 'Multicom - MUL-MN-V-TR - Optical Micro-Node', '11.jpg', '600.00', 13, 3, 'MUL-MN-V-TR', 1, 0, '', '2018-12-14 09:02:00', '2018-12-14 09:02:00'),
(17, 'Multicom – MNSC-5M-6F-6C-SC/APC – Node Service Cable', '12.jpg', '150000.00', 12, 4, 'MNSC-5M-6F-6C-SC/APC', 0, 0, '', '2018-12-14 09:02:33', '2018-12-14 09:02:33'),
(20, 'Pro Brand Eagle Aspen - D-2200 - Diplexer - Price Drop!', '13.jpg', '500.00', 11, 5, 'D-2200 ', 0, 1, '', '2018-12-23 10:15:26', '2018-12-23 10:15:26'),
(21, 'Multicom – MUL-MN-V-R – Optical Micro-Node Receiver', '14.jpg', '5600.00', 10, 6, ' MUL-MN-V-R', 0, 1, '', '2018-12-23 10:20:49', '2018-12-23 10:20:49'),
(22, 'Multicom - MUL-MN-V-TR-HP - High Power Optical Micro-Node', '15.jpg', '600.00', 9, 7, 'MUL-MN-V-TR-HP', 0, 1, '', '2018-12-23 10:21:21', '2018-12-23 10:21:21'),
(23, '\r\nMulticom – MUL-OFN-V-M-FP-4-M – Four-Port Outdoor Optical Node', '16.jpg', '7100', 8, 8, 'MUL-OFN-V-M-FP-4-M ', 0, 1, '', '2018-12-23 10:22:18', '2018-12-23 10:22:18'),
(24, 'Multicom – MUL-1.0M-KU – 1 Meter DTH KU-Band Satellite Dish', '17.jpg', '550.00', 7, 2, ' MUL-1.0M-KU ', 0, 0, '', '2018-12-23 10:23:02', '2018-12-23 10:23:02'),
(25, 'Multicom – MUL-2.4M-C_BAND – 2.4 Meter Prime Focus Satellite Dish', '18.jpg', '5200.00', 6, 4, ' MUL-2.4M-C_BAND', 0, 0, '', '2018-12-23 10:44:14', '2018-12-23 10:44:14'),
(26, 'Pico Macom – SIRD-FTA-2 – Free-to-Air Digital Satellite Receiver', '19.jpg', '3500.00', 1, 6, 'SIRD-FTA-2', 0, 0, '', '2018-12-23 11:07:17', '2018-12-23 11:07:17'),
(27, 'Pico Macom - MCM-201T - Preamplifier, Indoor, 20 db w/Tilt, 54-300 MHz', '20.jpg', '25000.00', 1, 8, 'MCM-201T', 0, 0, '', '2018-12-23 11:07:24', '2018-12-23 11:07:24'),
(28, 'Televes – 149981 – DAT-790 HD Boss, UHF Antenna (w/ PSU)', '21.jpg', '1300.00', 2, 1, '149981 – DAT-790 HD Bos', 0, 0, '', '2018-12-23 11:07:30', '2018-12-23 11:07:30'),
(29, 'AFL - FM00075 - LightLink, Poli-MOD - Patch and Splice Module', '22.jpg', '3200.00', 2, 3, 'FM00075', 0, 0, '', '2018-12-23 11:07:35', '2018-12-23 11:07:35'),
(30, 'AFL – FM-000752 – LightLink Poli-MOD Pigtailed Optical LightLink Interconnect Module', '23.jpg', '3500.00', 1, 5, 'FM-000752', 0, 0, '', '2018-12-23 11:07:41', '2018-12-23 11:07:41'),
(31, 'Multicom - FOAP-12-SM-D-LC/APC - Adapter Panel, Duplex, 12 LC/APC Connectors', '24.jpg', '5500.00', 2, 7, 'FOAP-12-SM-D-LC/APC ', 0, 0, '', '2018-12-23 11:07:47', '2018-12-23 11:07:47'),
(32, '\r\nDasan Zhone Solutions – MESH-2100-NA – Dual Band WiFi Repeater', '2.jpg', '780.00', 1, 3, 'MESH-2100-NA ', 0, 0, '', '2018-12-23 11:10:39', '2018-12-23 11:10:39'),
(33, 'Myra\r\nParty Wear Bow Bellies For Women', '21.jpg', '890.00', 1, 6, 'ABC', 0, 0, '', '2018-12-23 11:10:45', '2018-12-23 11:10:45'),
(34, 'Zaveri Pearls Combo of 4 Zinc Drop Earring', '20.jpg', '1250.00', 2, 5, 'ABC', 0, 0, '', '2018-12-23 11:10:52', '2018-12-23 11:10:52'),
(35, 'Jockey Women\'s Top', '24.jpg', '1500.00', 3, 4, 'ABC', 0, 0, '', '2018-12-23 11:10:57', '2018-12-23 11:10:57'),
(36, 'Kcher Churi', '2.jpg', '350.00', 4, 7, 'ABC', 0, 0, '', '2018-12-23 11:11:03', '2018-12-23 11:11:03'),
(37, 'Remanika\r\nWomen Fit and Flare Red, Black, White Dress', '10.jpg', '550.00', 5, 7, 'ABC', 0, 0, '', '2018-12-23 11:11:11', '2018-12-23 11:11:11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
