<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>MG Technologies</title>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="">
        <meta name="description" content="">
        <!-- animate -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.min.css">
        <!-- bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
        <!-- font-awesome -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
        <!-- google font -->
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,700,800' rel='stylesheet' type='text/css'>
        <!-- custom -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/menu.css">
    </head>
    <body data-spy="scroll" data-offset="50" data-target=".navbar-collapse">
        <!-- start navigation -->
        <div class="navbar navbar-fixed-top navbar-default" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <a href="#" class="navbar-brand"><img src="<?php echo base_url(); ?>assets/images/logo.png" width="350" class="img-responsive" alt="logo"></a>
                </div>
                <div class="navbar-right navbar_search">
                    <form action="<?php echo base_url();?>product/search">
                        <input type="text" placeholder="Search.." name="s">
                        <button type="submit"><i class="fa fa-search"></i></button>
                    </form>
                </div>
            </div>
            <div class="offset_bg">
                <?php include("menu.php");?>
            </div>
        </div>
        <div class="main_body">

            <!-- end navigation -->