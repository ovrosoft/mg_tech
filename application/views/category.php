<!-- start team -->
<div id="team">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-md-12">
                <h2 class="wow bounce"><?php echo $category->name; ?></h2>
            </div>
            <?php $m = 1;
            foreach ($product_list as $product) { ?>
                <div class="col-md-3 col-sm-3 wow fadeIn" data-wow-delay="0.<?php echo $m++; ?>s">
                    <div class="product box_shado">
                        <div class="img">
                            <img src="<?php echo base_url(); ?>assets/images/product/<?php echo $product->image; ?>"
                                 class="img-responsive" alt="<?php echo $product->name; ?>">
                        </div>
                        <h3><?php echo $product->name; ?></h3>
                        <h4>BRAND : <?php echo $product->brand; ?></h4>
                        <h4>MODEL : <span class="model_color"><?php echo $product->model_no; ?></span></h4>
                        <p><?php echo $product->description; ?></p>
                        <ul class="social-icon text-center">
                            <li><a href="#" class="wow fadeInUp fa fa-facebook" data-wow-delay="2s"></a></li>
                            <li><a href="#" class="wow fadeInDown fa fa-twitter" data-wow-delay="2s"></a></li>
                            <li><a href="#" class="wow fadeIn fa fa-instagram" data-wow-delay="2s"></a></li>
                            <li><a href="#" class="wow fadeInUp fa fa-pinterest" data-wow-delay="2s"></a></li>
                        </ul>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<!-- end team -->