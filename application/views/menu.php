<div class="a2z">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="responsive-title">MG TECHNOLOGIES</div>
                <div class="a2z-mega-menu">
                    <ul class="nav-list">
                        <li class="hc-li float-left border-left text-center"><a href="<?php echo base_url(); ?>"><i
                                        class="fa fa-home fa-i"></i>HOME</a>
                        </li>
                        <li class="hc-li float-left border-left text-center"><a href="#"><i
                                        class="fa fa-camera fa-i"></i>CATV</a>
                            <ul class="nav-submenu single-down text-left">
                                <?php foreach (get_categories("catv") as $item) { ?>
                                    <li><a class="border-bottom border-left border-right"
                                           href="<?php echo base_url(); ?>product/category/<?php echo $item->category_id; ?>"><i
                                                    class="fa <?php echo $item->icon; ?>"></i> <?php echo $item->name; ?>
                                        </a></li>
                                <?php } ?>
                            </ul>
                        </li>

                        <li class="hc-li float-left border-left text-center"><a href="#"><i
                                        class="fa fa-newspaper-o fa-i"></i>NETWORKING</a>
                            <ul class="nav-submenu single-down text-left">
                                <?php foreach (get_categories("networking") as $item) { ?>
                                    <li><a class="border-bottom border-left border-right"
                                           href="<?php echo base_url(); ?>product/category/<?php echo $item->category_id; ?>"><i
                                                    class="fa <?php echo $item->icon; ?>"></i> <?php echo $item->name; ?>
                                        </a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li class="hc-li float-left border-left text-center"><a href="<?php echo base_url(); ?>service"><i
                                        class="fa fa-dedent fa-i"></i>SERVICE</a>
                        </li>
                        <li class="hc-li float-left border-left text-center"><a href="<?php echo base_url(); ?>about"><i
                                        class="fa fa-archive fa-i"></i>ABOUT</a>
                        </li>
                        <li class="hc-li float-left border-left text-center"><a href="<?php echo base_url(); ?>portfolio"><i
                                        class="fa fa-cube fa-i"></i>PORTFOLIO</a>
                        </li>
                        <li class="hc-li float-left border-left border-right text-center"><a href="<?php echo base_url(); ?>contact"><i
                                        class="fa fa-phone fa-i"></i>CONTACT</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>