<!-- start home -->
<section id="home" class="text-center">
    <div class="templatemo_headerimage">
        <div class="flexslider">
            <ul class="slides">
                <li>
                    <img src="<?php echo base_url(); ?>assets/images/slider/1.jpg" alt="Slide 1">
                    <div class="slider-caption">
                        <div class="templatemo_homewrapper">
                            <h1 class="wow fadeInDown" data-wow-delay="2000">Solution Company</h1>
                            <h2 class="wow fadeInDown" data-wow-delay="2000">
                                <span>WE DESIGN &AMP; CODE WEBSITES</span>
                            </h2>
                        </div>
                    </div>
                </li>
                <li>
                    <img src="<?php echo base_url(); ?>assets/images/slider/2.jpg" alt="Slide 2">
                    <div class="slider-caption">
                        <div class="templatemo_homewrapper">
                            <h1 class="wow fadeInDown" data-wow-delay="2000">Flex Slider, CSS Flexbox</h1>
                            <h2 class="wow fadeInDown" data-wow-delay="2000">
                                <span>work on all modern browsers, Works on IE 10+</span>
                            </h2>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</section>
<!-- end home -->
<!-- start team -->
<div id="team">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-md-12">
                <h2 class="wow bounce">Top Rated Products</h2>
            </div>
            <?php $i = 1;
            foreach ($top_list as $top) { ?>
                <div class="col-md-3 col-sm-3 wow fadeIn" data-wow-delay="0.<?php echo $i++; ?>s">
                    <div class="product box_shado">
                        <div class="img ">
                            <img src="<?php echo base_url(); ?>assets/images/product/<?php echo $top->image; ?>"
                                  alt="<?php echo $top->name; ?>">
                        </div>
                        <h3><?php echo $top->name; ?></h3>
                        <h4>BRAND : <?php echo $top->brand; ?></h4>
                        <h4>MODEL : <span class="model_color"><?php echo $top->model_no; ?></span></h4>
                        <p><?php echo $top->description; ?></p>
                        <ul class="social-icon text-center">
                            <li><a href="#" class="wow fadeInUp fa fa-facebook" data-wow-delay="2s"></a></li>
                            <li><a href="#" class="wow fadeInDown fa fa-twitter" data-wow-delay="2s"></a></li>
                            <li><a href="#" class="wow fadeIn fa fa-instagram" data-wow-delay="2s"></a></li>
                            <li><a href="#" class="wow fadeInUp fa fa-pinterest" data-wow-delay="2s"></a></li>
                        </ul>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>


<div id="team">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-md-12">
                <h2 class="wow bounce">Best Selling Products</h2>
            </div>
            <?php $m = 1;
            foreach ($best_list as $best) { ?>
                <div class="col-md-3 col-sm-3 wow fadeIn" data-wow-delay="0.<?php echo $m++; ?>s">
                    <div class="product box_shado">
                        <div class="img ">
                            <img src="<?php echo base_url(); ?>assets/images/product/<?php echo $best->image; ?>"
                                 class="img-responsive" alt="<?php echo $best->name; ?>">
                        </div>
                        <h3><?php echo $best->name; ?></h3>
                        <h4>BRAND : <?php echo $best->brand; ?></h4>
                        <h4>MODEL : <span class="model_color"><?php echo $best->model_no; ?></span></h4>
                        <p><?php echo $best->description; ?></p>
                        <ul class="social-icon text-center">
                            <li><a href="#" class="wow fadeInUp fa fa-facebook" data-wow-delay="2s"></a></li>
                            <li><a href="#" class="wow fadeInDown fa fa-twitter" data-wow-delay="2s"></a></li>
                            <li><a href="#" class="wow fadeIn fa fa-instagram" data-wow-delay="2s"></a></li>
                            <li><a href="#" class="wow fadeInUp fa fa-pinterest" data-wow-delay="2s"></a></li>
                        </ul>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>