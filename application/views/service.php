<!-- start service -->
<div id="service">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="wow bounce text-center">Our services at a glance</h2>
            </div>
            <br>
            <div class="col-md-4 col-sm-4">
                <div class="media">
                    <div class="media-object media-left wow fadeIn" data-wow-delay="0.1s">
                        <i class="fa fa-laptop"></i>
                    </div>
                    <div class="media-body wow fadeIn">
                        <h3 class="media-heading">CATV</h3>
                        <p>Solution is a website template from Tooplate. You can download, edit and use this for any purpose.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="media">
                    <div class="media-object media-left wow fadeIn" data-wow-delay="0.3s">
                        <i class="fa fa-cog"></i>
                    </div>
                    <div class="media-body wow fadeIn">
                        <h3 class="media-heading">Networking</h3>
                        <p>Solution is a website template from Tooplate. You can download, edit and use this for any purpose.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="media">
                    <div class="media-object media-left wow fadeIn" data-wow-delay="0.6s">
                        <i class="fa fa-paper-plane"></i>
                    </div>
                    <div class="media-body wow fadeIn" data-wow-delay="0.3s">
                        <h3 class="media-heading">Security System</h3>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euism.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="media">
                    <div class="media-object media-left wow fadeIn" data-wow-delay="0.9s">
                        <i class="fa fa-html5"></i>
                    </div>
                    <div class="media-body wow fadeIn" data-wow-delay="0.6s">
                        <h3 class="media-heading">Retail Sales</h3>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euism.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="media">
                    <div class="media-object media-left wow fadeIn" data-wow-delay="0.4s">
                        <i class="fa fa-comments-o"></i>
                    </div>
                    <div class="media-body wow fadeIn" data-wow-delay="0.3s">
                        <h3 class="media-heading">Direct Sales</h3>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euism.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="media">
                    <div class="media-object media-left wow fadeIn" data-wow-delay="0.8s">
                        <i class="fa fa-check-circle"></i>
                    </div>
                    <div class="media-body wow fadeIn" data-wow-delay="0.6s">
                        <h3 class="media-heading">Up to Date</h3>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euism.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end service -->

<!-- start team -->
<div id="team">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-md-12">
                <h2 class="wow bounce">Our Enterprise Network and CA TV Project Support Products</h2>
            </div>
            <div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.3s">
                <div class="product">
                    <div class="img box_shado">
                        <img src="<?php echo base_url(); ?>assets/images/services/cable.jpg"
                             class="img-responsive" alt="">
                    </div>
                    <h4>Networking</h4>
                    <p>Apply our highly compatible and scalable basic enterprise network device to realize the connection between your business and the world.</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.2s">
                <div class="product">
                    <div class="img box_shado">
                        <img src="<?php echo base_url(); ?>assets/images/services/fiver.jpg"
                             class="img-responsive" alt="">
                    </div>
                    <h4>CA TV Cabling</h4>
                    <p>Choose our PoE network devices to enjoy lower cost, less downtime, easier maintenance, and greater installation flexibility than with traditional wiring.</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.3s">
                <div class="product">
                    <div class="img box_shado">
                        <img src="<?php echo base_url(); ?>assets/images/services/kvm.jpg"
                             class="img-responsive" alt="">
                    </div>
                    <h4>KVM Technology</h4>
                    <p>Gain efficiency and more control with our diverse KVM switches that provide a cohesive, unified and simplified solution to manage your devices.</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.4s">
                <div class="product">
                    <div class="img box_shado">
                        <img src="<?php echo base_url(); ?>assets/images/services/power.jpg"
                             class="img-responsive" alt="">
                    </div>
                    <h4>Power System</h4>
                    <p>Without power, even the modern data center can break down. Power system can help to provide sustainable and efficient electricity for your IT equipments.</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.4s">
                <div class="product">
                    <div class="img box_shado">
                        <img src="<?php echo base_url(); ?>assets/images/services/cable.jpg"
                             class="img-responsive" alt="">
                    </div>
                    <h4>Copper Cabling</h4>
                    <p>Properly designed end-to-end copper cabling systems including pre-terminated cable assemblies and tools offer high quality and quick deployment.</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.4s">
                <div class="product">
                    <div class="img box_shado">
                        <img src="<?php echo base_url(); ?>assets/images/services/fiver.jpg"
                             class="img-responsive" alt="">
                    </div>
                    <h4>Fiber Cabling</h4>
                    <p>Take advantage of optical fiber systems to speed up your business with higher bandwidth, easier management and less space.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end team -->