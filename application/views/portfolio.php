<!-- start portfolio -->
<div id="portfolio">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2 class="wow bounce">Recent Products</h2>
                <div class="iso-section wow fadeIn" data-wow-delay="0.6s">

                    <ul class="filter-wrapper clearfix">
                        <li><a href="#" data-filter="*" class="selected opc-main-bg">All</a></li>
                        <li><a href="#" class="opc-main-bg" data-filter=".graphic">CA TV</a></li>
                        <li><a href="#" class="opc-main-bg" data-filter=".photoshop">Networking</a></li>
                        <li><a href="#" class="opc-main-bg" data-filter=".wallpaper">Security System</a></li>
                    </ul>

                    <div class="iso-box-section">
                        <div class="iso-box-wrapper col4-iso-box">

                            <div class="iso-box graphic photoshop wallpaper col-md-4 col-sm-6 col-xs-12">
                                <div class="portfolio-thumb">
                                    <img src="<?php echo base_url(); ?>assets/images/portfolio-img1.jpg"
                                         class="fluid-img" alt="portfolio img">
                                    <div class="portfolio-overlay">
                                        <a href="#" class="fa fa-search"></a>
                                        <a href="#" class="fa fa-link"></a>
                                        <h4>Project title</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
                                            nonumm.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="iso-box graphic wallpaper col-md-4 col-sm-6 col-xs-12">
                                <div class="portfolio-thumb">
                                    <img src="<?php echo base_url(); ?>assets/images/portfolio-img2.jpg"
                                         class="fluid-img" alt="portfolio img">
                                    <div class="portfolio-overlay">
                                        <a href="#" class="fa fa-search"></a>
                                        <a href="#" class="fa fa-link"></a>
                                        <h4>Project title</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
                                            nonumm.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="iso-box wallpaper col-md-4 col-sm-6 col-xs-12">
                                <div class="portfolio-thumb">
                                    <img src="<?php echo base_url(); ?>assets/images/portfolio-img3.jpg"
                                         class="fluid-img" alt="portfolio img">
                                    <div class="portfolio-overlay">
                                        <a href="#" class="fa fa-search"></a>
                                        <a href="#" class="fa fa-link"></a>
                                        <h4>Project title</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
                                            nonumm.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="iso-box graphic col-md-4 col-sm-6 col-xs-12">
                                <div class="portfolio-thumb">
                                    <img src="<?php echo base_url(); ?>assets/images/portfolio-img4.jpg"
                                         class="fluid-img" alt="portfolio img">
                                    <div class="portfolio-overlay">
                                        <a href="#" class="fa fa-search"></a>
                                        <a href="#" class="fa fa-link"></a>
                                        <h4>Project title</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
                                            nonumm.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="iso-box wallpaper col-md-4 col-sm-6 col-xs-12">
                                <div class="portfolio-thumb">
                                    <img src="<?php echo base_url(); ?>assets/images/portfolio-img5.jpg"
                                         class="fluid-img" alt="portfolio img">
                                    <div class="portfolio-overlay">
                                        <a href="#" class="fa fa-search"></a>
                                        <a href="#" class="fa fa-link"></a>
                                        <h4>Project title</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
                                            nonumm.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="iso-box graphic photoshop col-md-4 col-sm-6 col-xs-12">
                                <div class="portfolio-thumb">
                                    <img src="<?php echo base_url(); ?>assets/images/portfolio-img6.jpg"
                                         class="fluid-img" alt="portfolio img">
                                    <div class="portfolio-overlay">
                                        <a href="#" class="fa fa-search"></a>
                                        <a href="#" class="fa fa-link"></a>
                                        <h4>Project title</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
                                            nonumm.</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- end portfolio -->