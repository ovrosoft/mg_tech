<!-- start about -->
<div id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 wow fadeInLeft" data-wow-delay="0.9s">
                <h3>About MG Technolgies</h3>
                <h4>Smart, Light and Adaptive</h4>
                <p>With passion and innovation, MG Technologies has grown to be a global leading communications hardware and project solutions provider. Based on strong hardware development, we are offering the most cost-effective infrastructure solutions on optical transport network, data center, enterprise network, and OEM solution. The number of our global customers has grown over the years. Also, we offer a variety of fast delivery services through our global warehouse locations.</p>
            </div>
            <div class="col-md-6 col-sm-6 wow fadeInRight" data-wow-delay="0.9s">
                <span class="text-top">Direct Sales <small>100%</small></span>
                <div class="progress">
                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                </div>
                <span>Retail Sales <small>80%</small></span>
                <div class="progress">
                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;"></div>
                </div>
                <span>Networking <small>90%</small></span>
                <div class="progress">
                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%;"></div>
                </div>
                <span>CA TV <small>70%</small></span>
                <div class="progress">
                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%;"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end about -->
<!-- start team -->
<div id="team">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-md-12">
                <h2 class="wow bounce">What We Do</h2>
                <p>e deliver high-value products, solutions and services to help you build your communication system and infrastructure to achieve your objectives.</p>
                <br>
            </div>
            <div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay=".3s">
                <div class="about">
                    <div class="img">
                        <img src="<?php echo base_url(); ?>assets/images/about/data.png"
                             class="img-responsive" alt="">
                    </div>
                    <h3>CA TV Products</h3>
                    <p>Provide data platform for networks across private, public and hybrid clouds.</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay=".6s">
                <div class="about">
                    <div class="img">
                        <img src="<?php echo base_url(); ?>assets/images/about/network.png"
                             class="img-responsive" alt="">
                    </div>
                    <h3>Networking System</h3>
                    <p>Support independent data processing, storage, transmission and management..</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay=".9s">
                <div class="about">
                    <div class="img">
                        <img src="<?php echo base_url(); ?>assets/images/about/catv.png"
                             class="img-responsive" alt="">
                    </div>
                    <h3>Data Center Network</h3>
                    <p>Provide data platform for networks across private, public and hybrid clouds.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end team -->
<!-- start team -->
<div id="team">
    <div class="container-fluid">
        <div class="row no-gutters">
            <div class="col-md-12">
                <h2 class="wow bounce">Our Trusted Partners</h2>
            </div>
            <div class="col-md-12 col-sm-12 wow fadeIn" data-wow-delay="">
               <div class="row">
                  <div class="col-xl-3 col-sm-3 ">
                      <img class="about_logo" src="<?php echo base_url(); ?>assets/images/about/1.png">
                        
                   </div>

                    <div class="col-xl-3 col-sm-3 ">
                      <img class="about_logo" src="<?php echo base_url(); ?>assets/images/about/2.png">
                        
                   </div>
                    <div class="col-xl-3 col-sm-3 ">
                      <img class="about_logo" src="<?php echo base_url(); ?>assets/images/about/3.png">
                        
                   </div>

                    <div class="col-xl-3 col-sm-3 ">
                      <img class="about_logo" src="<?php echo base_url(); ?>assets/images/about/4.png">
                        
                   </div>
               </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- end team -->

             