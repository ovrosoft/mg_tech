</div>
<!-- start footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-7">
                <p>Copyright &copy; <?php echo date("Y");?> MG Technologies</p>
                <small>Designed & Developed by <a href="https://www.ovrosoft.com">Ovrosoft Limited</a></small>
            </div>
            <div class="col-md-4 col-sm-5">
                <ul class="social-icon">
                    <li><a href="https://www.facebook.com/Zakir-Electronics-And-MG-Technologies-166161447146231/" class="fa fa-facebook"></a></li>
                    <li><a href="#" class="fa fa-twitter"></a></li>
                    <li><a href="#" class="fa fa-instagram"></a></li>
                    <li><a href="#" class="fa fa-pinterest"></a></li>
                    <li><a href="#" class="fa fa-google"></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- end footer -->


<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
<!-- bootstrap -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<!-- isotope -->
<script src="<?php echo base_url(); ?>assets/js/isotope.js"></script>
<!-- images loaded -->
<script src="<?php echo base_url(); ?>assets/js/imagesloaded.min.js"></script>
<!-- wow -->
<script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
<!-- smoothScroll -->
<script src="<?php echo base_url(); ?>assets/js/smoothscroll.js"></script>
<!-- jquery flexslider -->
<script src="<?php echo base_url(); ?>assets/js/jquery.flexslider.js"></script>
<!-- custom -->
<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>

</body>
</html>