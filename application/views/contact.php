<!-- start contact -->
<div id="contact">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-4 wow fadeInLeft" data-wow-delay="0.6s">
                <h2><strong>MG</strong> Technologies.</h2>
                <p>Quisque at elit sapien. Sed velit enim, fringilla non suscipit quis, tristique et turpis. Proin vitae nisi justo.</p>
                <ul class="social-icon">
                    <li><a href="#" class="fa fa-facebook"></a></li>
                    <li><a href="#" class="fa fa-twitter"></a></li>
                    <li><a href="#" class="fa fa-instagram"></a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-4 wow fadeIn" data-wow-delay="0.9s">
                <address>
                    <h3>Our Office</h3>
                    <p><i class="fa fa-map-marker too-icon"></i> Showroom : Shop-32, Bangabandhu Stadium Super Market, Dhaka 1000, Bangladesh</p>
                    <p><i class="fa fa-phone too-icon"></i> 02-47111869</p>
                    <p><i class="fa fa-envelope-o too-icon"></i> info@mgtechbd.com</p>
                </address>
            </div>
            <form action="#" method="get" class="col-md-6 col-sm-4" id="contact-form" role="form">
                <div class="col-md-6 col-sm-12 wow fadeIn" data-wow-delay="0.3s">
                    <input name="txt_name" type="text" class="form-control" id="txt_name" placeholder="Name" required>
                </div>
                <div class="col-md-6 col-sm-12 wow fadeIn" data-wow-delay="0.3s">
                    <input name="email" type="email" class="form-control" id="txt_email" placeholder="Email" required>
                </div>
                <div class="col-md-12 col-sm-12 wow fadeIn" data-wow-delay="0.9s">
                    <textarea name="txt_message" rows="5" class="form-control" id="txt_message" placeholder="Message" required></textarea>
                </div>
                <div class="col-md-9 col-sm-6 wow fadeIn" data-wow-delay="0.3s">
                    <div id="email-success"></div>
                </div>
                <div class="col-md-3 col-sm-6 wow fadeIn" data-wow-delay="0.3s">
                    <input name="submit" type="submit" class="form-control pull-right" id="submit" value="Send">
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end contact -->

<!-- start google map -->
<div class="google_map">
    <div id="map-canvas"></div>
</div>
<!-- end google map -->