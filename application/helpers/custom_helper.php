<?php defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('get_categories')) {

    function get_categories($type)
    {
        $ci =& get_instance();
        $ci->load->model("category_model");

        $cat = $ci->category_model->getCategories(array("type" => $type));

        $category = array();

        if ($cat) {
            $category = $cat->result();
        }
        return $category;
    }
}