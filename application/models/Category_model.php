<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends CI_Model
{

    public function getCategories($params = array())
    {

       $this->db->select("*")
            ->from("category");

        if (isset($params["type"])) {
            $this->db->where("type", $params["type"]);
        }

        if (isset($params["categoryId"])) {
            $this->db->where("category_id", $params["categoryId"]);
        }

        $query = $this->db->order_by("category_id", "ASC")
            ->get();

        return $query->num_rows() > 0 ? $query : false;
    }

}
