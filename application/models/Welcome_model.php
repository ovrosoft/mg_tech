<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome_model extends CI_Model
{

    public function topRatedList()
    {

        $query = $this->db->select("p.*, b.name brand")
            ->from("product p")
            ->join("brand b", "p.brand_id=b.brand_id", "left")
            ->where("p.top_rated", 1)
            ->limit(8)
            ->order_by("p.product_id", "DESC")
            ->get();

        return $query->num_rows() > 0 ? $query : false;
    }

    public function bestSellingList()
    {

        $query = $this->db->select("p.*, b.name brand")
            ->from("product p")
            ->join("brand b", "p.brand_id=b.brand_id", "left")
            ->where("p.best_selling", 1)
            ->limit(8)
            ->order_by("p.product_id", "DESC")
            ->get();

        return $query->num_rows() > 0 ? $query : false;
    }

}
