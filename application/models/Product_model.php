<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model
{

    public function getProducts($params = array())
    {

        $this->db->select("p.*, b.name brand")
            ->from("product p")
            ->join("brand b", "p.brand_id=b.brand_id", "left");

        if (isset($params["search"])) {
            $this->db->group_start()
                ->or_like("p.name", $params['search'])
                ->or_like("p.model_no", $params['search'])
                ->or_like("b.name", $params['search'])
                ->group_end();
        }

        if (isset($params["categoryId"])) {
            $this->db->where("category_id", $params["categoryId"]);
        }

        $query = $this->db->limit(16)
            ->order_by("p.product_id", "DESC")
            ->get();


        return $query->num_rows() > 0 ? $query : false;
    }

}
