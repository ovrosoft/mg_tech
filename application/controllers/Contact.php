<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller
{

    public function index()
    {
        $this->load->view('header');
        $this->load->view('contact');
        $this->load->view('footer');
    }

    public function send_mail()
    {
        $data=array(
            "from_name"=>$this->input->post('txt_name',true),
            "from_email"=>$this->input->post('txt_email',true),
            "txt_message"=>$this->input->post('txt_message',true)
        );

        $temp=$this->load->view("email", $data, true);

        $this->load->library('email');
        $this->email->from($data["from_email"], $data["from_name"])
            ->to("khalifa.shahin@gmail.com")
            ->cc("info@mgtechbd.com")
            ->bcc("fewlinkit@gmail.com")
            ->subject("This Email Sent from Customer")
            ->message($temp)
            ->send();
    }

}
