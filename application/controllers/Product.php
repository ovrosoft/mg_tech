<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller
{

    public function category($categoryId = 0)
    {
        $this->load->model(array("product_model", "category_model"));

        $category = $this->category_model->getCategories(array("categoryId" => $categoryId));
        if (!$category) {
            redirect(base_url(), 'refresh');
        }

        $data["product_list"] = array();
        $product = $this->product_model->getProducts($categoryId);

        if ($product) {
            $data["product_list"] = $product->result();
        }

        $data["category"] = $category->row();

        $this->load->view('header')
            ->view('category', $data)
            ->view('footer');
    }

    public function search()
    {
        $text = $this->input->get("s");
        $this->load->model("product_model");

        $data["product_list"] = array();
        $product = $this->product_model->getProducts(array("search" => trim($text)));

        if ($product) {
            $data["product_list"] = $product->result();
        }

        $this->load->view('header')
            ->view('search', $data)
            ->view('footer');
    }
}
