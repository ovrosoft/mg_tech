<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{

    public function index()
    {
        $this->load->model("welcome_model");

        $data["top_list"] = array();
        $data["best_list"] = array();

        $top = $this->welcome_model->topRatedList();
        if ($top) {
            $data["top_list"] = $top->result();
        }

        $best = $this->welcome_model->bestSellingList();
        if ($best) {
            $data["best_list"] = $best->result();
        }

        $this->load->view('header')
            ->view('welcome', $data)
            ->view('footer');
    }

}
